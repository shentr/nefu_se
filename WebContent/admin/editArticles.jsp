<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
<div id="articles-listDiv">
	<table id="articles-list"></table>
</div>
<div class="form-group" id="editArticles-edit">
	<div class="row row-margin-bottom">
		<input type="hidden" id="editArticles-articleId"></input>
		<label for="editArticles-type" class="col-sm-1 text-right">类别：</label>
		<div class="col-sm-11">
			<select class="form-control" id="editArticles-type" disabled="true">
				<option value="0">专业概况</option>
				<option value="1">师资人员</option>
				<option value="10">师资人员 - 全职教师</option>
				<option value="11">师资人员 - 兼职客座</option>
				<option value="12">师资人员 - 博士后</option>
				<option value="2">本科生</option>
				<option value="20">本科生 - 优秀毕业生</option>
				<option value="3">研究生</option>
				<option value="30">研究生 - 学生名单</option>
				<option value="4">优秀毕业生</option>
				<option value="5">学术活动</option>
				<option value="6">新闻事件</option>
				<option value="7">硬件设施</option>
			</select>
		</div>
	</div>
	
	<div class="row row-margin-bottom">
		<label for="editArticles-title" class="col-sm-1 text-right">标题：</label>
		<div class="col-sm-11">
			<input type="text" id="editArticles-title" placeholder="请输入标题..." class="form-control">
		</div>
	</div>
	<script id="editorEdit" type="text/plain" style="height:400px;"></script>
	<div class="row row-margin-bottom"></div>
	<div class="row row-margin-bottom">
		<div class="col-sm-3 col-sm-offset-2">
    		<input class="form-control  btn btn-primary" type="button" id="editArticles-submit" value="保存修改">
		</div>
		<div class="col-sm-3 col-sm-offset-1">
    		<input class="form-control  btn btn-default" type="button" id="editArticles-back" value="返回列表">
		</div>
	</div>
</div>
<script>
function editArticlesReady() {
	$('#articles-list').bootstrapTable('refresh');
	$('#articles-listDiv').show();
	$('#editArticles-edit').hide();
}
var ue2 = UE.getEditor('editorEdit');
function editArticles_delete(articleId) {
	$.post("admin/ArticlesOperation?action=delete",
            {
                id: articleId
            },
            function (data) {
                if(data=='success') {
                	$('#articles-list').bootstrapTable('refresh');
                	$('#AlertP').html('删除成功');
                    $('#ErrorAlert').modal('show');
                }
                else {
                	$('#AlertP').html(data);
                    $('#ErrorAlert').modal('show');
                }
            }
    );
}
function editArticles_edit(articleId) {
	$('#articles-listDiv').hide();
	$.post("admin/ArticlesOperation?action=get",
            {
                id: articleId
            },
            function (data) {
            	var result = JSON.parse(data);
                if (result.type != -1) { //如果等于-1，说明错误
                	$('#editArticles-type').val(result.type);
                    $('#editArticles-title').val(result.title);
                    ue2.setContent('');
                    ue2.execCommand('insertHtml', result.html);
                    $('#editArticles-articleId').val(articleId);
                    $('#editArticles-edit').show();
                }
                else{
                    $('#AlertP').html("系统出错");
                    $('#ErrorAlert').modal('toggle');
                }
            }
    );
}
$(function() {
	$('#editArticles-submit').click(function() {
		$.post("admin/ArticlesOperation?action=update",
	            {
					id: $('#editArticles-articleId').val(),
	                title: $('#editArticles-title').val(),
	                text: ue2.getAllHtml()
	            },
	            function (data) {
	                if(data=='success') {
	                	//ue2.setContent('');
	                	//$('#editArticles-title').val('');
	                	$('#AlertP').html('保存成功');
	                    $('#ErrorAlert').modal('show');
	                }
	                else {
	                	$('#AlertP').html(data);
	                    $('#ErrorAlert').modal('show');
	                }
	            }
	    );
	});
	$('#editArticles-back').click(function(){
		$('#editArticles-edit').hide();
		$('#articles-listDiv').show();
	});
	$('#articles-list').bootstrapTable({
        classes: 'table table-striped table-condensed table-hover',
        method: 'get',
        url: 'admin/editArticlesListPost',
        search:true,
        columns: [{
            field: 'id',
            title: 'ID',
            align: 'center',
            width: '10%',
            searchable: true
        }, {
            field: 'title',
            title: '标题',
            align: 'center',
            width: '30%',
        }, {
            field: 'type',
            title: '类别',
            align: 'center',
            width: '30%',
        }, {
            field: 'op',
            title: '操作',
            align: 'center',
            width: '30%',
        }],
		pagination: true,
		sidePagination:'client',
		pageSize: 15
    });
});
</script>