<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page language="java" import="java.util.HashMap" %>
<%@ page language="java" import="com.javaWeb.Article" %>
<%@ page language="java" import="com.javaWeb.ArticlesList" %>
<%@ page language="java" import="com.javaWeb.ArticleTypeMap" %>
<%@ page language="java" import="com.javaWeb.GetArticles" %>
<%
Article atc;
GetArticles gatc;
int id,type,typeId,firstLevelType,firstLevelTypeId;
try{
	id = Integer.valueOf(request.getParameter("id")).intValue();
	atc = new Article(id);
	type = atc.type;
	//out.write(String.format("<div>%d</div>", type));
	firstLevelType= type<10 ? type : type/10;
	gatc=new GetArticles();
	gatc.setArtList(type<10 ? type : firstLevelType*10);
	gatc.setArticle(0);
	firstLevelTypeId=gatc.getArticle().id;
	gatc.setArtList(type);
	gatc.setArticle(0);
	typeId=gatc.getArticle().id;
	request.setAttribute("title",atc.title);
	request.setAttribute("id",id);
	request.setAttribute("type",atc.type);
}catch(Exception e) {
	e.printStackTrace();
	return;
}
%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
<link type="text/css" rel="stylesheet" href="res/iiis/css/bootstrap.css">
<link type="text/css" rel="stylesheet" href="res/iiis/css/style.css">
<link type="text/css" rel="stylesheet" href="res/iiis/css/tree-menu-style.css">
<link type="text/css" rel="stylesheet" href="res/iiis/css/erji.css">

<!-- jQuery -->
<script src="res/iiis/js/jquery.min.js"></script>

<title>东北林业大学-软件工程</title>
<meta name="keywords" content="">
<meta name="description" content="">
<!-- <script type="text/javascript">
	var cookie_pre = "LRy_";
	var cookie_domain = '.tsinghua.edu.cn';
	var cookie_path = '/';
	var web_url = 'http://iiis.tsinghua.edu.cn/';
</script> -->
<script type="text/javascript" src="res/js/base.js"></script>
</head>
<body>
	<div class="article-nav">
		<jsp:include page="common/topNav.jsp"/>
	</div>
	<div class="article-nav">
	<jsp:include page="common/nav.jsp"/>
	</div>
	<div class="container">
		<ol class="breadcrumb color_777">
			<li class="color_999">您所在的位置</li>
<!--当前位置*************************************************************  -->
			<%
			out.write("<li><a href=\"#\">文章</a></li>");
			String str="<li><a href=\"article.jsp?id=%d\">%s</a></li>";
			ArticleTypeMap typeMap=new ArticleTypeMap();
			HashMap<String,String> map = typeMap.typeToName();
			if(type<10){
				out.write(String.format(str, firstLevelTypeId, map.get(firstLevelType)));
			}else{
				out.write(String.format(str, firstLevelTypeId, map.get(firstLevelType)));
				out.write(String.format(str, typeId, map.get(type)));	
			}
			out.write(String.format(str, id, atc.title));
			%>
			<!-- <li><a href="index.jsp"></a></li> -->
		</ol>
	</div>
	<div class="main-content">
		<div class="container">
			<div class="row">
				<div class="col-md-3 left-caidan">
					<div class="left-title">
						<!-- <span class="wd"><a href="/institutes/"> 科研机构</a></span> -->
						<%
						str="<span class=\"wd\"><a href=\"article.jsp?id=%d\">%s</a></span>";
						out.write(String.format(str, firstLevelTypeId,map.get(firstLevelType)));
						%>
					</div>
					<div class="left-list">
						<ul class="cd-accordion-menu animated">
						<%
						//str = "<li><a href=\"article.jsp?id=%d\" class=\"active\"><strong>%s</strong></a></li>";
						str="<li class=\"has-children\"><input type=\"checkbox\" name=\"list-%d\" id=\"list-%d\" checked=\"\"><label for=\"list-%d\"><a href=\"article.jsp?id=%d\">%s</a></label><ul>"; 
						for(int i=firstLevelType*10;i<firstLevelType*10+9;i++){
							if(i<10) break;
							if(map.containsKey(i)){				/*一级导航  */
								gatc.setArtList(i);
								gatc.setArticle(0);
								firstLevelTypeId=gatc.getArticle().id;
								out.write(String.format(str, i, i, i, firstLevelTypeId, map.get(i)));
		/*二级导航  */
								for(int j=0;j<gatc.getArtList().list.size();j++){
									gatc.setArticle(j);
									if(gatc.getArticle().id == id){
										out.write(String.format("<li><a class=\"cd-choose\" href=\"article.jsp?id=%d\">%s</a></li>",gatc.getArticle().id, gatc.getArticle().title));
									}else{
										out.write(String.format("<li><a href=\"article.jsp?id=%d\">%s</a></li>",gatc.getArticle().id, gatc.getArticle().title));
									}
								}
								out.write("</ul></li>");
							}
						}
						%>
						
						
						
						<% 
							str = "<li><a href=\"article.jsp?id=%d\"><strong>%s</strong></a></li>";
							gatc.setArtList(firstLevelType);
							for(int i=0;i<gatc.getArtList().list.size();i++) {
								/* Article article = artList.list.get(i); */
								gatc.setArticle(i);
								if(gatc.getArticle().id == id){
									out.write(String.format("<li class=\"cd-choose\"><a href=\"article.jsp?id=%d\"><strong>%s</strong></a></li>",gatc.getArticle().id,gatc.getArticle().title));
								}else{
									out.write(String.format(str,gatc.getArticle().id,gatc.getArticle().title));
								}
								//out.write(String.format(str,gatc.getArticle().id,gatc.getArticle().title));
							}
						%>			
						</ul>
					</div>
				</div>
				<div class="col-md-9">
					<div class="contentss">
						<h4 class="color_danger text-center">${title }</h4>
						<div class="dbline-s margin_bottom20 margin_top20"></div>
						<div class="embed-responsive embed-responsive-4by3" style="height:48em">
  							<iframe id="edui271_iframe" scrolling="yes" frameborder="0" src="articleText.jsp?id=${id }">
							</iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<jsp:include page="common/footer.jsp" />
</body>
</html>