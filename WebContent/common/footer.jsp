<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div class="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-4 margin_bottom20">
					<hr>
					<h5>
						<strong>清华大学交叉信息研究院</strong>
					</h5>
					<hr>
					<p>
						地址: 北京市 海淀区 清华大学<br>信息科学技术楼(FIT楼) 1区208室<br> 邮编: 100084<br>
						电话: 010-62781693<br> 传真: 010-62797331-2000<br> 邮件:
						iiis@tsinghua.edu.cn
					</p>
				</div>
				<div class="col-md-2 margin_bottom20">
					<hr>
					<h5>
						<strong>其他链接</strong>
					</h5>
					<hr>
					<div class="list-group">
						<a href="http://www.csail.mit.edu/" class="list-group-item "
							target="_blank"><span class="badge"> &raquo;</span>CSAIL </a> <a
							href="http://www.itcsc.cuhk.edu.hk/" class="list-group-item "><span
							class="badge" target="_blank"> &raquo;</span>ITCSC</a> <a
							href="http://www.tsinghua.edu.cn/" class="list-group-item "><span
							class="badge" target="_blank"> &raquo;</span>清华大学</a>
					</div>
				</div>
				<div class="col-md-2 margin_bottom20">
					<hr>
					<h5>
						<strong>院系链接</strong>
					</h5>
					<hr>
					<div class="list-group">
						<a href="http://library.iiis.tsinghua.edu.cn/"
							class="list-group-item " target="_blank"><span class="badge">
								&raquo;</span>院系图书馆 </a> <a href="#" class="list-group-item "><span
							class="badge" target="_blank"> &raquo;</span>院系网关</a> <a href="#"
							class="list-group-item "><span class="badge" target="_blank">
								&raquo;</span>院系Wiki</a> <a href="#" class="list-group-item "><span
							class="badge" target="_blank"> &raquo;</span>院系FTP</a>

					</div>
				</div>
				<div class="col-md-4 margin_bottom20">
					<hr>
					<h5>
						<strong>分院链接</strong>
					</h5>
					<hr>
					<dl class="dl-horizontal">
						<dt>ITCS</dt>
						<dd>
							<a href="/list-370-1.html">理论计算机科学研究中心</a>
						</dd>
						<dt>CQI</dt>
						<dd>
							<a href="/show-4468-1.html">量子信息中心 </a>
						</dd>
						<dt>CTCS</dt>
						<dd>
							<a href="/list-372-1.html">清华大学-麻省理工学院-香港中文大学理论计算机科学研究中心</a>
						</dd>
						<dt>JCQI</dt>
						<dd>
							<a href="/list-373-1.html">清华-密西根-量子信息联合中心 </a>
						</dd>
						<dt>CTIC</dt>
						<dd>
							<a href="http://ctic.au.dk/">清华-奥胡斯交互计算理论中心 </a>
						</dd>
						<dt>JCQC</dt>
						<dd>
							<a href="/list-404-1.html">清华-滑铁卢量子计算联合中心 </a>
						</dd>
					</dl>
				</div>
			</div>
		</div>
		<div class="footer-cp">
			版权所有 @ 清华大学交叉信息研究院 &nbsp;访问次数: <span id="web_pv_num">载入中...</span>
		</div>
	</div>
	<!-------------------------------------->
	<!----- js ----->
	<!-------------------------------------->
	<script src="res/iiis/js/bootstrap.min.js"></script>
	<script src="res/iiis/js/bootstrap-hover-dropdown.min.js"></script>
	<script src="res/iiis/js/my.js"></script>
	<script src="res/iiis/js/tree-menu.js"></script>
	<link rel="stylesheet" href="res/js/dialog/ui-dialog.css" />
	<script src="res/js/dialog/dialog-plus.js"></script>
	<script type="text/javascript"
		src="http://iiis.tsinghua.edu.cn/index.php?f=pv"></script>
	<script src="res/js/wz_tooltip.js"></script>
	<script type="text/javascript" src="res/iiis/js/marquee.js"></script>
	<script type="text/javascript">
    var _uid=getcookie('_uid');
    if(_uid!=null) {
        $("#mylogined").removeClass('hide');
        $("#mylogin").addClass('hide');
        var _username=decodeURI(getcookie('truename'));
        $("#myname").html(_username);
    }
</script>


	