package com.javaWeb;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Article {
	public String title = "";
	public String text = "";
	public String createTime="";               									  ////////////////////////////////
	public int type=-1;
	public int id = -1;
	public Article(int id) {
		connect con=null;
		try {
			con = new connect();
			ResultSet rs = con.stmt.executeQuery("select * from article where id="+id);
			while (rs.next()) {
				title = rs.getString("title");
				text = rs.getString("html");
				type = rs.getInt("type");
				createTime=rs.getString("create_time");						//////////////////////////
                //System.out.println(rs.getString(1) + "\t" + rs.getString(2));// 入如果返回的是int类型可以用getInt()
            }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			con.finalize();
		}
	}
	public Article(int id,String title) {
		this.title = title;
		this.id = id;
	}
																			////////////////////////////////////////////////////////////////////////////////////
	public Article(int id,String title,int type,String createTime){
		this.title = title;
		this.id = id;
		this.type=type;
		this.createTime=createTime;
	}
}
