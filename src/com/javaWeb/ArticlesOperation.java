package com.javaWeb;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Servlet implementation class ArticlesOperation
 */
@WebServlet("/ArticlesOperation")
public class ArticlesOperation extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ArticlesOperation() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		String action = request.getParameter("action");
		if(action.equals("get")) { //获取文章，返回json字符串
			String id = request.getParameter("id");
			connect con = new connect();
			ResultSet rs;
			JSONObject obj = new JSONObject();
			try {
				rs = con.stmt.executeQuery("select * from article where id="+id);
				if(rs.next()) {
					obj.put("id", id);
					obj.put("title", rs.getString("title"));
					obj.put("type", rs.getInt("type"));
					obj.put("html",rs.getString("html"));
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				obj.put("type", -1);
				e.printStackTrace();
			} finally{
				response.getWriter().write(obj.toString());
			}
		}
		else if(action.equals("delete")) { //删除文章
			String id = request.getParameter("id");
			connect con = new connect();
			try {
				ResultSet rs = con.stmt.executeQuery("select type from article where id="+id);
				if(rs.next()) {
					int type = rs.getInt("type");
					rs = con.stmt.executeQuery("select count(*) as count from article where type="+type);
					if(rs.next()) {
						if(rs.getInt("count")<=1) {
							response.getWriter().write("此分类下只剩余一篇文章，无法删除");
							return;
						}
					}
				}
				else {
					response.getWriter().write("参数不正确");
					return;
				}
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				response.getWriter().write(e1.getMessage());
				return;
			}
			
			
			try {
				con.stmt.executeUpdate("delete from article where id="+id);
				response.getWriter().write("success");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				response.getWriter().write("遇到错误，删除失败");
				e.printStackTrace();
			}
		}
		else if(action.equals("update")) {
			String id = request.getParameter("id");
			String title = request.getParameter("title");
	        String text = request.getParameter("text");
	        if(title==null||text==null) {
	        	response.getWriter().write("参数错误");
	        	return;
	        }
	        else if(title.equals("")) {
	        	response.getWriter().write("标题不能为空");
	        	return;
	        }
	        connect con = new connect();
	        if(con.updateArticle(id, title, text)){
	        	response.getWriter().write("success");
	        }
	        else {
	        	response.getWriter().write("未知错误");
	        }
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
