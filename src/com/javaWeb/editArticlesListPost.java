package com.javaWeb;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Servlet implementation class editArticlesListPost
 */
@WebServlet("/editArticlesListPost")
public class editArticlesListPost extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public editArticlesListPost() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setCharacterEncoding("UTF-8");
		JSONArray obj = new JSONArray();
		JSONObject member;
		connect con = new connect();
		String sql = "select * from article";
		try {
			ResultSet rs = con.stmt.executeQuery(sql);
			ArticleTypeMap typeMap = new ArticleTypeMap(1);
			while (rs.next()) {
				member = new JSONObject();
				String type = typeMap.getName(rs.getInt("type"));
				int id = rs.getInt("id");
				member.put("id", id);
				member.put("title", rs.getString("title"));
				member.put("type", type);
				member.put("op","<a onclick='editArticles_delete("+id+")'>删除</a>&nbsp;&nbsp;&nbsp;<a onclick='editArticles_edit("+id+")'>编辑</a>");
				obj.put(member);
			}
			response.getWriter().write(obj.toString());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			con.finalize();
		}
	}
}
