package com.javaWeb;

import java.util.HashMap;

public class ArticleTypeMap /*extends HttpServlet*/{
	private HashMap map = new HashMap();
	public ArticleTypeMap() {
		map.put(0,"专业概况");
		map.put(1,"师资人员");
		map.put(10,"全职教师");
		map.put(11,"兼职客座");
		map.put(12,"博士后");
		map.put(2,"本科生");
		map.put(20,"优秀毕业生");
		map.put(21,"学生活动");
		map.put(3,"研究生");
		map.put(30,"学生名单");
		map.put(4,"优秀毕业生");
		map.put(5,"学术活动");
		map.put(6,"新闻事件");
		map.put(7,"硬件设施");
	}
	public ArticleTypeMap(int extension) {
		if(extension==1) {
			map.put(0,"专业概况");
			map.put(1,"师资人员");
			map.put(10,"师资人员-全职教师");
			map.put(11,"师资人员-兼职客座");
			map.put(12,"师资人员-博士后");
			map.put(2,"本科生");
			map.put(20,"本科生-优秀毕业生");
			map.put(21,"本科生-学生活动");
			map.put(3,"研究生");
			map.put(30,"研究生-学生名单");
			map.put(4,"优秀毕业生");
			map.put(5,"学术活动");
			map.put(6,"新闻事件");
			map.put(7,"硬件设施");
		}
	}
	public String getName(int type) {
		return (String) map.get(type);
	}
	public HashMap typeToName() {
		return map;
	}
}
